module.exports = {
	API_URL: process.env.NEXT_PUBLIC_API_URL, //Variable for our API URL in our .env.local file
	getAccessToken: () => localStorage.getItem('token'), //function for getting our token from local Storage
	toJSON: (res) => res.json() //for converting response to JSON format
}