import React from 'react'
import Link from 'next/link';
import { Navbar, Nav } from 'react-bootstrap';

export default function NavBar() {
	return(
		<Navbar bg="light" expand="lg">
			<Link href='/'>
				<a className="navbar-brand">Budget Tracker</a>
			</Link>
			<Navbar.Toggle aria-controls="basic-navbar-nav" /> 
			<Navbar.Collapse id="basic-navbar-nav"> 
				<Nav className="mr-auto"> 
					<Link href="/categories">
						<a className="nav-link" role="button">Categories</a> 
					</Link> 
					<Link href="/records"> 
						<a className="nav-link" role="button">Records</a> 
					</Link> 
					<Link href="/monthlyExpenses"> 
						<a className="nav-link" role="button">Monthly Expenses</a> 
					</Link>
					<Link href="/monthlyIncome"> 
						<a className="nav-link" role="button">Monthly Income</a> 
					</Link> 
					<Link href="/trend"> 
						<a className="nav-link" role="button">Trend</a> 
					</Link> 
					<Link href="/breakdown"> 
						<a className="nav-link" role="button">Breakdown</a>
					</Link>
					<Link href="/logout"> 
						<a className="nav-link" role="button">Logout</a>
					</Link>  
				</Nav> 
			</Navbar.Collapse> 
		</Navbar> 
	)
}

//removed some /users ask about this