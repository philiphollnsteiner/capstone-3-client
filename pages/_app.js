import React, { useState, useEffect } from 'react';
import NavBar from '../components/NavBar';

import { UserProvider } from '../UserContext';
import AppHelper from '../app-helper';

import 'bootswatch/dist/simplex/bootstrap.min.css';
import '../styles/globals.css'

function MyApp({ Component, pageProps }) {

	const [user, setUser] = useState({
		id: null
	})
	useEffect(() => {
		const options ={
			headers: { Authorization: `Bearer ${ AppHelper.getAccessToken()}`}
		}

		fetch(`${ AppHelper.API_URL}/api/users/details` , options)
		.then(AppHelper.toJSON)
		.then(data => {

			if(typeof data._id !== 'undefined'){
				setUser({id: data._id})
			} else {
				setUser({id:null})
			}
		})
	},[])

	const unsetUser = () => {
		localStorage.clear()

		setUser({
			id:null
		})
	}

  return (
  	<UserProvider value={{user, setUser, unsetUser}}>
  		<NavBar/>
  		<Component {...pageProps} />
  	</UserProvider>
  )
}

export default MyApp
