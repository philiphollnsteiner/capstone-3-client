import React, {useState, useEffect} from 'react';
import { Form, Container } from 'react-bootstrap';
import { Pie } from 'react-chartjs-2';
import AppHelper from '../../app-helper';
import styles from '../../styles/Home.module.css';
import moment from 'moment';
import { colorRandomizer } from '../../helpers';

export default function Breakdown(){

	const [categoryName, setCategoryName] = useState([])
	const [amount, setAmount] = useState(0)
	const [startDate, setStartDate] = useState(moment().subtract(1,'months').format('YYYY-MM-DD'))
	const [endDate, setEndDate] = useState(moment().format('YYYY-MM-DD'))
	const [bgColors, setBgColors] = useState([])
	const data = {
		labels: categoryName,
		datasets: [
			{
				data: amount,
				backgroundColor: bgColors,
				hoverBackgroundColor: bgColors
			}
		]
	}

		useEffect(() => { 

			fetch(`${ AppHelper.API_URL }/api/users/get-records-breakdown-by-range`, {
						method: 'POST',
						headers: {
							'Content-Type': 'Application/json',
							'Authorization': `Bearer ${ AppHelper.getAccessToken()}`
						},
						body: JSON.stringify(
							{
								fromDate: startDate,
								toDate: endDate
							}
						)
					})
					.then(AppHelper.toJSON)
					.then(recordsData => {
						console.log(recordsData)

							setCategoryName(recordsData.map(record => record.categoryName))
							setAmount(recordsData.map(record => record.totalAmount))
							setBgColors(recordsData.map(() => `#${colorRandomizer()}`))
					})
		},[])
		//console.log(recordsData)
	
	return(
		<React.Fragment>
			<Container>
				<Form.Group controlId='startDate'>
					<Form.Label>Search Date Start:</Form.Label>
					<Form.Control type="date" value={startDate} onChange={e => setStartDate(e.target.value)}/>
				</Form.Group>
				<Form.Group controlId='endDate'>
					<Form.Label>Search Date End:</Form.Label>
					<Form.Control type="date" value={endDate} onChange={e => setEndDate(e.target.value)}/>
				</Form.Group>
			<Pie data={data}/>
			</Container>
		</React.Fragment>
	)
}

