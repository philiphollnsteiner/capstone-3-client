import React, { useState, useEffect } from 'react';
import { Form, Button, Card, Container, Col } from 'react-bootstrap';
import Head from 'next/head';
import AppHelper from '../../app-helper'
import Swal from 'sweetalert2';
import Router from 'next/router';

export default function AddCategory(){

	const [categoryName, setCategoryName] = useState("")
	const [categoryType, setCategoryType] = useState(undefined)
	const [isActive, setIsActive] = useState(false)

	useEffect(() => {
		if(categoryName !== "" && categoryType !== undefined){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [categoryName, categoryType])

	function createCategory(e){
		e.preventDefault()

		const options = {
			method: 'POST',
			headers: { 
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${AppHelper.getAccessToken()}`
			},
			body: JSON.stringify({
				name: categoryName,
				typeName: categoryType
			}) 
		}
		
		fetch(`${ AppHelper.API_URL }/api/users/add-category`, options)
		.then(AppHelper.toJSON)
		.then(data => {
			//console.log(data)
			if(data == true){
				Swal.fire(
					"Congradulations",
					"Category sucessfully created",
					"success"
				)
			}else{
				Swal.fire(
					"Category add Failed",
					"Something went wrong",
					"error"
				)
			}
			
		})
		console.log(`${categoryName} has been created with a category type of ${categoryType}`)

		Router.push('/categories')
	}

	return(
		<React.Fragment>
			<Head>
				<title>Categories</title>
			</Head>
			<Container>
				<Card>
					<Col className = "py-3">
						<h4>Category Information</h4>
						<br/>
						<Form onSubmit={e => createCategory(e)}>
							<Form.Group controlId="categoryName">
								<Form.Label>Category Name:</Form.Label>
								<Form.Control type="text" placeholder ="Enter Category Name" value ={categoryName} onChange={e => setCategoryName(e.target.value)} required/>
							</Form.Group>
							<Form.Group controlId="categoryType">
								<Form.Label>Select Category Type:</Form.Label>
								<Form.Control as="select" value={categoryType} onChange={e => setCategoryType(e.target.value)} reqired>
									<option selected disabled>Select Category</option>
									<option value="Income" >Income</option>
									<option value="Expense" >Expense</option>
								</Form.Control>
							</Form.Group>
							{isActive === true
							?
							<Button className="bg-info" type="submit">Submit</Button>
							:
							<Button className="bg-info" type="submit" disabled>Submit</Button>
						}
						</Form>
					</Col>
				</Card>
			</Container>
			
		</React.Fragment>
	)
}