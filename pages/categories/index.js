import React, { useEffect, useState } from 'react';
import { Container, Table, Button } from 'react-bootstrap';
import Head from 'next/head';
import AppHelper from '../../app-helper';

export default function Categories(){

	const [categories, setCategories] = useState([])
	const [remove, setRemove] = useState(false)

	useEffect(() => {
		const options = {
			method: 'POST',
			headers: { 
				'Authorization' : `Bearer ${AppHelper.getAccessToken()}`
			}

		}
		fetch(`${ AppHelper.API_URL }/api/users/get-categories`, options)
		.then(AppHelper.toJSON)
		.then(categoryData =>{
			//console.log(categoryData)
			
			const categoriesRows = categoryData.map(category => {

			return (
				<tr key={category._id}>
					<td>{category.name}</td>
					<td>{category.type}</td>
					<td>
						<Button variant="warning">Update</Button>
	                   	<Button variant="danger" value ={remove} onChange={e => setRemove(e.target.true)}>Delete</Button>
	                   	{console.log(remove)}
					</td>
				</tr> 
			)
			})
			console.log(categoriesRows)
			setCategories(categoriesRows)
		})
	},[])

		function del(e){
			e.preventDefault()

			const options = {
					method: 'DELETE',
					headers: { 
						'Authorization' : `Bearer ${AppHelper.getAccessToken()}`
					}
			}

			fetch(`${ AppHelper.API_URL }/api/users/:_id`, options)
			.then(AppHelper.toJSON)
			.then(res => console.log(res))
		}

	useEffect(() => {
		if(remove == true){
			del()
		}else{
			null
		}
	}, [remove])

	return(
		<React.Fragment>
			<Head>
				<title>Categories</title>
			</Head>
			<Container>
				<h3>Categories</h3>
				<Button href="/categories/add">Add</Button>
			</Container>
			<br/>
			<Container>
				<Table striped bordered hover>
					<thead>
						<tr>
							<th>Category</th>
							<th>Type</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						{categories}
					</tbody>
				</Table>
			</Container>
		</React.Fragment>
	)
}