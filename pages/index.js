import { Row, Col, Card, Container } from 'react-bootstrap';

export default function Home(){
	return(
		<Container>
			<Row>
				<Col xs={12} md={4}>
					<Card className="cardHighlight">
						<Card.Body>

							<Card.Title>
								<h2>View Your Categories</h2>
							</Card.Title>

							<Card.Text>
							Categories can help you organize your finances. Find out how much money you spend on everyday items.		
							</Card.Text>
							<a href='/categories'>Click here to view your categories</a>
						</Card.Body>
					</Card>
				</Col>
				<Col xs={12} md={4}>
					<Card className="cardHighlight">
						<Card.Body>

							<Card.Title>
								<h2>View Your Records</h2>
							</Card.Title>

							<Card.Text>
							Records clearly show you your current finances. Take control of your finances by adding a record. 		
							</Card.Text>
							<a href='/records'>Click here to view your records</a>
						</Card.Body>
					</Card>
				</Col>
				<Col xs={12} md={4}>
					<Card className="cardHighlight">
						<Card.Body>

							<Card.Title>
								<h2>View Your Trends</h2>
							</Card.Title>

							<Card.Text>
							Seeing your spending habits will help you see your financial progress over time. See how well you are doing at saving over time.		
							</Card.Text>
							<a href='/trend'>Click here to view your trends</a>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}
