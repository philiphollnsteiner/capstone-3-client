import React, { useState, useContext } from 'react';
import { Form, Button, Card, Container } from 'react-bootstrap';
import { GoogleLogin } from 'react-google-login';
import Router from 'next/router';
import Swal from 'sweetalert2';
import AppHelper from '../../app-helper';
import UserContext from '../../UserContext';

export default function Login(){
	
	const { setUser } = useContext(UserContext)
	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")

	function authenticate(e){
		e.preventDefault()

		const options = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json'},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}

		fetch(`${AppHelper.API_URL}/api/users/login`, options)
		.then(AppHelper.toJSON)
		.then(data => {
			//console.log(data)
			if(typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access)
				
				retrieveUserDetails(data.access)

				Swal.fire(
					'Welcome Back',
					'Login Successful',
					'success'
				)
			}else{
				if(data.error === "does-not-exist"){
					Swal.fire(
						'Authentication Failed',
						'User does not exist',
						'error'
					)
				}else if(data.error === "incorrect-password"){
					Swal.fire(
						'Authentication Failed',
						'Password is not correct',
						'error'
					)
				}else if (data.error === "login-type-error"){
					Swal.fire(
						'Authentication Failed',
						'Use different login procedure',
						'error'
					)
				}
			}
		})
	}

	function authenticateGoogleToken(response){

		const payload ={
			method : 'POST',
			headers : { 'Content-Type': "application/json"},
			body : JSON.stringify({ tokenId : response.tokenId })
		}

		fetch(`${ AppHelper.API_URL }/api/users/verify-google-token-id`, payload)
		.then(AppHelper.toJSON)
		.then(data=>{
			//console.log(data)
			if(typeof data.access !== 'undefined'){
				localStorage.setItem('token', data.access)
				localStorage.setItem('googleToken', response.accessToken)
				retrieveUserDetails(data.access)

				Swal.fire(
					'Welcome Back',
					'Login Successful',
					'success'
				)
			}else{

				if(data.error === "login-type-error"){
					Swal.fire(
  					'Authentication Failed',
  					'User not registered with google',
  					'error'
					)
				}else if(data.error === "google-auth-error"){

					Swal.fire(
  					'Authentication Failed',
  					'Google authentiaction error',
  					'error'
  					)
  				}
			}
		})
	}

	function retrieveUserDetails(accessToken){
		const options = {
			headers: {Authorization: `Bearer ${ accessToken }`}
		}

		fetch(`${ AppHelper.API_URL }/api/users/details`, options)
		.then(AppHelper.toJSON)
		.then(data=> {

			setUser({ id: data._id })
		})
			Router.push('/')
		
	}

	return(
		<React.Fragment>
		<Card>
			<Card.Header>Login Details</Card.Header>
			<Card.Body>
			<Container>
				<Form onSubmit={e => authenticate(e)}> 

					<Form.Group controlId="userEmail">
						<Form.Label>Email:</Form.Label>
						<Form.Control type="email" placeholder="Enter your email" value={email} onChange={e => setEmail(e.target.value)} required/>
					</Form.Group>

					<Form.Group controlId="password">
						<Form.Label>Password:</Form.Label>
						<Form.Control type="password" placeholder="Enter your password" value={password} onChange={e => setPassword(e.target.value)} required/>
					</Form.Group>

					<Button variant="primary" type="submit" block>Submit</Button>
					<br/>
					<GoogleLogin
						clientId="387252318241-v0su2llu40qsti4fnq569q3v8ef1d41a.apps.googleusercontent.com"
						buttonText="Login"
						onSuccess={authenticateGoogleToken}
						onFailure={authenticateGoogleToken}
						cookiePolicy={'single_host_origin'}
						className="w-100 text-center d-flex justify-content-center"
					/>
				</Form>
			</Container>
			</Card.Body>
			<p class="paragraph">Not Registered? Register <a href="/register">Here</a></p>
		</Card>
		
		</React.Fragment>
	)
}