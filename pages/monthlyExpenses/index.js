import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import { Bar } from 'react-chartjs-2';
import AppHelper from '../../app-helper';
import moment from 'moment';
import { Container } from 'react-bootstrap';

export default function MonthlyExpenses(){
	
	const [monthlyAmount, setMonthlyAmount] = useState(0)
	const [startDate, setStartDate] = useState(moment().subtract(1,'years').format('YYYY-MM-DD'))
	const [endDate, setEndDate] = useState(moment().format('YYYY-MM-DD'))
	const [expenseData, setExpenseData] = useState([])
	const [month, setMonth] = useState([])
	const monthsRef = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
	

	const data ={
		labels: month,
		datasets: [{
			label: 'Monthly Expenses',
			backgroundColor: 'rgba(249, 147, 147, 0.2)',
			borderWidth: 1,
			hoverBackgroundColor: "rgba(251, 186, 186, 0.2)",
			hoverBorderColor: "rgba(251, 186, 186, 0.2)",
			data: monthlyAmount,
		}],
		
	}

	useEffect(() => {

		fetch(`${ AppHelper.API_URL }/api/users/get-records-by-range`, {
			method: 'POST',
			headers: {
				'Content-Type': 'Application/json',
				'Authorization': `Bearer ${ AppHelper.getAccessToken()}`
			},
			body: JSON.stringify(
				{
					fromDate: startDate,  
					toDate: endDate
				}
			)
		})
		.then(AppHelper.toJSON)
		.then(recordsData => {
			//console.log(recordsData)
			const filteredExpenseData = recordsData.filter(data => data.type === "Expense")
			//console.log(filteredExpenseData)
			setExpenseData(filteredExpenseData)
		})
	},[])
	console.log(expenseData)
	useEffect(() => {
		if(expenseData.length > 0){
			
			let tempMonths = [];

			expenseData.forEach(element => {

				if(!tempMonths.find(month => month === moment(element.dateAdded).format('MMMM'))){

					tempMonths.push(moment(element.dateAdded).format('MMMM'))
				}
			})

			const monthsRef = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
			
			tempMonths.sort((a,b) => {

				if(monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1){

					return monthsRef.indexOf(a) - monthsRef.indexOf(b)
				}
			})

			setMonth(tempMonths)
		}

	}, [expenseData])
		console.log(month)
	useEffect(() => {

		setMonthlyAmount(month.map(month => {

			let amount = 0

			expenseData.forEach(element => {
				if(moment(element.dateAdded).format('MMMM') === month){
					amount = amount + parseInt(element.amount)
				}
			})
			return amount 
		}))
		
	},[month])
		console.log(monthlyAmount)

	return(
		<Container>
			<Bar data={data}/>
		</Container>
	)
}