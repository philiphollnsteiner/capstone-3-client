import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import { Bar } from 'react-chartjs-2';
import AppHelper from '../../app-helper';
import moment from 'moment';
import { Container } from 'react-bootstrap';

export default function MonthlyIncome(){
	
	const [monthlyAmount, setMonthlyAmount] = useState(0)
	const [startDate, setStartDate] = useState(moment().subtract(1,'years').format('YYYY-MM-DD'))
	const [endDate, setEndDate] = useState(moment().format('YYYY-MM-DD'))
	const [incomeData, setIncomeData] = useState([])
	const [month, setMonth] = useState([])
	const monthsRef = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
	

	const data ={
		labels: month,
		datasets: [{
			label: 'Monthly Income',
			backgroundColor: 'rgba(131, 246, 132, 0.2)',
			borderWidth: 1,
			hoverBackgroundColor: "rgba(159, 284, 160, 0.2)",
			hoverBorderColor: "rgba(159, 284, 160, 0.2)",
			data: monthlyAmount,
		}],
		
	}

	useEffect(() => {

		fetch(`${ AppHelper.API_URL }/api/users/get-records-by-range`, {
			method: 'POST',
			headers: {
				'Content-Type': 'Application/json',
				'Authorization': `Bearer ${ AppHelper.getAccessToken()}`
			},
			body: JSON.stringify(
				{
					fromDate: startDate,  
					toDate: endDate
				}
			)
		})
		.then(AppHelper.toJSON)
		.then(recordsData => {
			//console.log(recordsData)
			const filteredIncomeData = recordsData.filter(data => data.type === "Income")
			//console.log(filteredIncomeData)
			setIncomeData(filteredIncomeData)
		})
	},[])
	console.log(incomeData)
	useEffect(() => {
		if(incomeData.length > 0){
			
			let tempMonths = [];

			incomeData.forEach(element => {

				if(!tempMonths.find(month => month === moment(element.dateAdded).format('MMMM'))){

					tempMonths.push(moment(element.dateAdded).format('MMMM'))
				}
			})

			const monthsRef = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
			
			tempMonths.sort((a,b) => {

				if(monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1){

					return monthsRef.indexOf(a) - monthsRef.indexOf(b)
				}
			})

			setMonth(tempMonths)
		}

	}, [incomeData])
		console.log(month)
	useEffect(() => {

		setMonthlyAmount(month.map(month => {

			let amount = 0

			incomeData.forEach(element => {
				if(moment(element.dateAdded).format('MMMM') === month){
					amount = amount + parseInt(element.amount)
				}
			})
			return amount 
		}))
		
	},[month])
		console.log(monthlyAmount)

	return(
		<Container>
			<Bar data={data}/>
		</Container>
	)
}