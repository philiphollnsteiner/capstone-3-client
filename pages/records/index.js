import React, { useEffect, useState } from 'react';
import { Card, Button, Container, Table, Form } from 'react-bootstrap';
import AppHelper from '../../app-helper';
import moment from 'moment';


export default function Records(){
	
	const [records, setRecords] = useState([])
	const [searchRecord, setSearchRecord] = useState([])
	const [searchType, setSearchType] = useState("All")
	const [searchKeyword, setSearchKeyword] = useState("")
	
	function recentRecords(){

		fetch(`${ AppHelper.API_URL }/api/users/get-most-recent-records`, {
				method: 'POST',
				headers: {
					'Authorization': `Bearer ${ AppHelper.getAccessToken()}`
				}
			})
			.then(AppHelper.toJSON)
			.then(recordsData => {
				//console.log(recordsData)

				const mappedRecordsData = recordsData.map(record => {

				const date = moment(record.dateAdded).format('MM-DD-YYYY')
					return(
						<Card>
							<Card.Body>
								<Card.Title value={record.categoryName}>{record.categoryName}</Card.Title>
								<Card.Text>
									<span value={record.type}>{record.type}</span>
									<br/>
									<span value={record.description}>Description: {record.description}</span>
									<br/>
									<span value={record.amount}>Amount: {record.amount}</span>
									<br/>
									<span value={record.dateAdded}>Added On: {date}</span>
								</Card.Text>
							</Card.Body>
						</Card>
					)
				})
				setRecords(mappedRecordsData)
		})
	}
		

		function searchRecentRecord(){

			fetch(`${ AppHelper.API_URL }/api/users/search-record`, {
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json',
						'Authorization': `Bearer ${ AppHelper.getAccessToken()}`
					},
					body: JSON.stringify({
						searchType: searchType,
						searchKeyword: searchKeyword,
					})
				})
				.then(AppHelper.toJSON)
				.then(recordsData=> {
					console.log(recordsData)
					
					const mappedRecordsData = recordsData.map(record => {

					return(
						<Card className = "my-5">
							<Card.Body>
								<Card.Title value={record.categoryName}>{record.categoryName}</Card.Title>
								<Card.Text>
									<span value={record.type}>{record.type}</span>
									<br/>
									<span value={record.description}>Description: {record.description}</span>
									<br/>
									<span value={record.amount}>Amount: {record.amount}</span>
									<br/>
									<span value={record.dateAdded}>Added On: {record.dateAdded}</span>
								</Card.Text>
							</Card.Body>
						</Card>
					)
				})
				setRecords(mappedRecordsData)
				})
		}
		useEffect(() => {
			if(searchKeyword !=='' || searchType !== 'All'){
				searchRecentRecord()
			}else{
				recentRecords()
			}
			
		},[searchKeyword,searchType])

	return( 
		<React.Fragment>
			<Container>
				<h1>Records</h1>
				<br/>
				<Button href="/records/newRecord">Add</Button>
				<br/>
				<br/>
				<Form onSubmit={e => searchRecord(e)}>
					<Form.Group>
						<Form.Control
							type="text"
							placeholder="Search for specific record"
							value={searchKeyword}
							onChange={e=> setSearchKeyword(e.target.value)}
						/>
					</Form.Group>
				</Form>
				<Form onSubmit={e => searchRecord(e)}>
					<Form.Group controlId="categoryType">
						<Form.Label>Select Category Type:</Form.Label>
						<Form.Control as="select" value={searchType} onChange={e => setSearchType(e.target.value)}>
							<option selected value='All'>All</option>
							<option value="Income" >Income</option>
							<option value="Expense" >Expense</option>
						</Form.Control>
					</Form.Group>
				</Form>
				{records}
			</Container>
		</React.Fragment>
	)
}
