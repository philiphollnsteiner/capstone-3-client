import React, { useState, useEffect } from 'react';
import { Form, Button, Card, Container, Col } from 'react-bootstrap';
import Head from 'next/head';
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2';
import Router from 'next/router';

export default function AddRecord(){

	const [categoryType, setCategoryType] = useState(undefined)
	const [categoryName, setCategoryName] = useState(undefined)
	const [amount, setAmount] = useState(0)
	const [description, setDescription] = useState("")
	const [balanceAfterTransaction, setBalanceAfterTransaction] = useState(0)
	const [isActive, setIsActive] = useState(false)

	const [categoryNameOptions, setCategoryNameOptions] = useState([])

	useEffect(() => {
		const options = {
			method: 'POST',
			headers: {
				'Authorization' : `Bearer ${AppHelper.getAccessToken()}`
			}
		}

		fetch(`${ AppHelper.API_URL }/api/users/get-categories`, options)
		.then(AppHelper.toJSON)
		.then(categoryData =>{
			//console.log(categoryData)

			const mapCategoryData = categoryData.map(category => {
				return(
					<option key={category._id} value={category.name}>{category.name}</option>
				)
			})
			
			//console.log(mapCategoryData)
			setCategoryNameOptions(mapCategoryData)
		})

	},[])

	useEffect(() => {
		if(categoryType !== undefined && categoryName !== undefined && amount !== 0){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	},[categoryType, categoryName, amount])

	function createRecord(e){
		e.preventDefault()

		const options = {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${AppHelper.getAccessToken()}`
			},
			body: JSON.stringify({
				typeName: categoryType,
				categoryName: categoryName,
				amount: amount,
				description: description,
				balanceAfterTransaction: balanceAfterTransaction
			})
		}

		fetch(`${ AppHelper.API_URL }/api/users/add-record`, options)
		.then(AppHelper.toJSON)
		.then(data => {
			console.log(data)
			if(data == true){
				Swal.fire(
					"Congradulations",
					"Record sucessfully created",
					"success"
				)
			}else{
				Swal.fire(
					"Record add failed",
					"Something went wrong",
					"error"
				)
			}
		})
		console.log(`New record of ${categoryName} was created with a type of ${categoryType} amount of ${amount} and a description of ${description}`)

		Router.push('/records')
	}

	return(
		<React.Fragment>
			<Head>
				<title>Add Record</title>
			</Head>
			<Container>
				<Card>
					<Col className = "py-3">
						<h4>New Record</h4>
						<br/>
						<Form onSubmit={e => createRecord(e)}>
							<Form.Group controlId="categoryType">
								<Form.Label>Category Type:</Form.Label>
								<Form.Control as="select" value={categoryType} onChange={e => setCategoryType(e.target.value)} required>
									<option selected disabled>Select Category Type</option>
									<option value="Income">Income</option>
									<option value="Expense">Expense</option>
								</Form.Control>
							</Form.Group>

							<Form.Group controlId="categoryName">
								<Form.Label>Category Name</Form.Label>
								<Form.Control as="select" value={categoryName} onChange={e => setCategoryName(e.target.value)} required>
									<option selected disabled>Select Category Name</option>
									{categoryNameOptions}
								</Form.Control>
							</Form.Group>

							<Form.Group controlId="amount">
								<Form.Label >Amount $:</Form.Label>
								<Form.Control type="number" value={amount} onChange={e => setAmount(parseFloat(e.target.value))} required/>
							</Form.Group>

							<Form.Group controlId="description">
								<Form.Label></Form.Label>
								<Form.Control type="text" placeholder="Enter Description Here" value={description} onChange={e => setDescription(e.target.value)}/>
							</Form.Group>
							{isActive === true
								?
							<Button className="bg-info" type="submit">Submit</Button>
								:
							<Button className="bg-info" type="submit" disabled>Submit</Button>
						}
						</Form>
					</Col>
				</Card>
			</Container>
		</React.Fragment>
	)
}