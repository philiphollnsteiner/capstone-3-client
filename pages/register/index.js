import React, { useState, useEffect, useContext } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import Head from 'next/head';
import Swal from 'sweetalert2';
import AppHelper from '../../app-helper';
import Router from 'next/router';
import UserContext from '../../UserContext';

export default function Register(){

	const { setUser } = useContext(UserContext) 
	const [email, setEmail] = useState("")
	const [name, setName] = useState("")
	const [password1, setPassword1] = useState("")
	const [password2, setPassword2] = useState("")
	const [isActive, setIsActive] = useState(false)

	useEffect(() => {
		if((email !== '' && password1 !== '' && password2 !== '')&&(password1 === password2)){
			setIsActive(true)
		}else{
			setIsActive(false)
		}

	}, [email, password1, password2])

	function registerUser(e){
		e.preventDefault()

		//console.log(`User ${email} successfully registered with password: ${password1}`)

		//fetch request and behavior to redirect here
		const options = {
			method : 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				email: email,
				name: name,
				password1: password1,
				password2: password2
			})
		}

		fetch(`${ AppHelper.API_URL}/api/users/register`, options)
		.then(AppHelper.toJSON)
		.then(data => {
			//console.log(data)

		Swal.fire(
			"Registration successful",
			"Lets get started",
			"success"
		)

		setUser({ id: data._id })
		//console.log(id)
		})
		Router.push('/')
		
	}

	return(
		<React.Fragment>
			<Head>
				<title>Registration</title>
			</Head>
			<Container>
				<Form onSubmit={e => registerUser(e)}>

					<Form.Group controlId="userEmail">
						<Form.Label>Email Address:</Form.Label>
						<Form.Control type="email" placeholder="Enter your email" value={email} onChange={e => setEmail(e.target.value)} required/>
					</Form.Group>

					<Form.Group controlId="name">
						<Form.Label>Name:</Form.Label>
						<Form.Control type="string" placeholder="Enter your first name" value={name} onChange={e => setName(e.target.value)} required/>
					</Form.Group>

					<Form.Group controlId="password1">
						<Form.Label>Password:</Form.Label>
						<Form.Control type="password" placeholder="Enter Your password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
					</Form.Group>

					<Form.Group controlId="password2">
						<Form.Label>Verify Password:</Form.Label>
						<Form.Control type="password" placeholder="Verify your password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
					</Form.Group>

					{isActive === true
						?<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
						:<Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
					}

				</Form>
			</Container>
		</React.Fragment>
	)
}