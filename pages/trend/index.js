import React, { useEffect,useState } from 'react';
import Head from 'next/head';
import { Line } from 'react-chartjs-2';
import AppHelper from '../../app-helper';
import moment from 'moment';
import { Container } from 'react-bootstrap';

export default function Trend(){

	const [amount, setAmount] = useState(0)
	const [startDate, setStartDate] = useState(moment().subtract(1,'months').format('YYYY-MM-DD'))
	const [endDate, setEndDate] = useState(moment().format('YYYY-MM-DD'))

	const data = {
		labels: amount,
		datasets: [
			{
				label: 'Account Trend',
				data: amount,
			}
		]
	}


	useEffect(() => { 

		fetch(`${ AppHelper.API_URL }/api/users/get-records-by-range`, {
			method: 'POST',
			headers: {
				'Content-Type': 'Application/json',
				'Authorization': `Bearer ${ AppHelper.getAccessToken()}`
			},
			body: JSON.stringify(
				{
					fromDate: startDate,  
					toDate: endDate
				}
			)
		})
		.then(AppHelper.toJSON)
		.then(recordsData => {
			//console.log(recordsData)
			setAmount(recordsData.map(record => record.balanceAfterTransaction))
		})
	},[])
				console.log(amount)
		return(
		<React.Fragment>
			<Head>
				<title>Budget Trend</title>
			</Head>
			<Container>
				<Line data = {data}/>
			</Container>
		</React.Fragment>
	)
}